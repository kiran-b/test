package com.hcl.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.model.Employee;
import com.hcl.model.EmployeeAddress;
import com.hcl.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@RequestMapping(value = "/empDetails", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Employee>> listOfEmpDetails() {
		List<Employee> employeeDetails = employeeService.listOfEmpDetails();
		if (employeeDetails.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(employeeDetails, HttpStatus.OK);
	}

	@RequestMapping(value = "/empDetail/{empId}", method = RequestMethod.GET,produces = "application/json")
	public ResponseEntity<Employee> findByempDetail(@PathVariable("empId") Long empId) {
		Employee employee = employeeService.findByempDetail(empId);
		if (employee == null) {
			return new ResponseEntity("No Customer found for ID " + empId, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity(employee, HttpStatus.OK);
	}

	@RequestMapping(value = "/saveEmpAddressDetails", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity saveEmployeeDetails(@RequestBody Employee employee) {
		int count = employeeService.saveEmployeeDetails(employee);
		if (count == 1) {
			return new ResponseEntity(HttpStatus.OK);
		} else {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}

	}
	
	@RequestMapping(value = "/saveEmployeeAddressDetails", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity saveEmployeeAddressDetails(@RequestBody Employee employee) {
		List<EmployeeAddress> empAddresses = employeeService.saveEmployeeAddressDetails(employee);
		if (empAddresses.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity(empAddresses,HttpStatus.OK);
		}

	}

}

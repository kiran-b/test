package com.hcl.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.hcl.model.Employee;
import com.hcl.model.EmployeeAddress;

@Component
@Qualifier("EmployeeDao")
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public List<Employee> listOfEmpDetails() {
		String sql = "SELECT * FROM EMPLOYEEDETAILS";
		List<Employee> listOfEmployeeDetails = new ArrayList<Employee>();
		List<Map<String, Object>> employeeList = jdbcTemplate.queryForList(sql);
		for (Map list : employeeList) {
			Employee employee = new Employee();
			employee.setEmpId((Long) list.get("empId"));
			employee.setEmpName((String) list.get("empName"));
			employee.setEmpDesignation((String) list.get("empDesignation"));
			employee.setEmpDOB((Date) list.get("empDOB"));
			listOfEmployeeDetails.add(employee);
		}
		return listOfEmployeeDetails;
	}

	/*
	 * @SuppressWarnings("unchecked") public Employee findByempDetail(Long
	 * empId) {
	 * 
	 * String sql = "SELECT * FROM EMPLOYEEDETAILS WHERE empId= ?"; Employee
	 * employee = (Employee) jdbcTemplate.queryForObject(sql, new Object[] {
	 * empId }, new EmployeeRowmapper());
	 * 
	 * return employee; }
	 */

	public Employee findByempDetail(Long empId) {
		String sql = " SELECT a.empId, a.empName,a.empDesignation,a.empDOB, b.street,b.area,b.pinCode FROM EMPLOYEEDETAILS a left outer join empaddress b  on a.empId = b.empId where a.empId=?";

		return jdbcTemplate.query(sql, new Object[] { empId }, new PersonResultSetExtractor());

	}

	public int saveEmployeeDetails(Employee employee) {
		String sql = "INSERT INTO EMPLOYEEDETAILS(empId,empName,empDesignation,empDOB)values(?,?,?,?)";
		int count = jdbcTemplate.update(sql, new Object[] { employee.getEmpId(), employee.getEmpName(),
				employee.getEmpDesignation(), employee.getEmpDOB() });
		// EmployeeAddress empAdd =employee.getEmployeeAddress();
		return count;
	}

	@Override
	public Long getEmployeeId() {
		String sql = "select max(empId) from EMPLOYEEDETAILS";
		Long empId = jdbcTemplate.queryForLong(sql);
		return empId;
	}


	public List<EmployeeAddress> saveEmployeeAddressDetails(Employee employee, Long empId) {
		String sql = "INSERT INTO EMPADDRESS(empId,street,area,pinCode)values(?,?,?,?)";
		List<EmployeeAddress> listOfEmpAddress=new ArrayList<>();
		List<Map<String, Object>> empAddressList =jdbcTemplate.queryForList(sql);
        for(Map listOfAdd :empAddressList ){
        	EmployeeAddress employeeAddress=new EmployeeAddress();
        	employeeAddress.setEmpId((Long)listOfAdd.get("empId"));
        	employeeAddress.setArea((String) listOfAdd.get("area"));
        	employeeAddress.setStreet((String)listOfAdd.get("street"));
        	employeeAddress.setPinCode((Long)listOfAdd.get("pinCode"));
        	listOfEmpAddress.add(employeeAddress);
        }
		return listOfEmpAddress;
	}
	}

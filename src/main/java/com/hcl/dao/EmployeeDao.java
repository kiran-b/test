package com.hcl.dao;

import java.util.List;

import com.hcl.model.Employee;
import com.hcl.model.EmployeeAddress;

public interface EmployeeDao {

	public List<Employee> listOfEmpDetails();

	public Employee findByempDetail(Long empId);
	
	public int saveEmployeeDetails(Employee employee);
	
	public List<EmployeeAddress> saveEmployeeAddressDetails(Employee employee, Long empId);

	public Long getEmployeeId();

}

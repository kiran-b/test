package com.hcl.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hcl.model.Employee;

public class EmployeeRowmapper implements RowMapper {

	public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		Employee employee = new Employee();
		employee.setEmpId(rs.getLong("empId"));
		employee.setEmpName(rs.getString("empName"));
		employee.setEmpDesignation(rs.getString("empDesignation"));
		employee.setEmpDOB(rs.getDate("empDOB"));

		return employee;
	}

}

package com.hcl.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.ResultSetExtractor;

import com.hcl.model.Employee;
import com.hcl.model.EmployeeAddress;

public class PersonResultSetExtractor implements ResultSetExtractor<Employee> {

	public Employee extractData(ResultSet rs) throws SQLException {
		Employee employee = null;
		while (rs.next()) {
			employee = new Employee();
			employee.setEmpId(rs.getLong("empId"));
			employee.setEmpName(rs.getString("empName"));
			employee.setEmpDesignation(rs.getString("empDesignation"));
			employee.setEmpDOB(rs.getDate("empDOB"));
			EmployeeAddress address = new EmployeeAddress();
			address.setArea(rs.getString("area"));
			address.setStreet(rs.getString("street"));
			address.setPinCode(rs.getLong("pincode"));
			address.setEmpId(rs.getLong("empId"));
			employee.setEmployeeAddress(address);
		}
		return employee;
	}

}

package com.hcl.service;

import java.util.List;

import com.hcl.model.Employee;
import com.hcl.model.EmployeeAddress;

public interface EmployeeService {
	public List<Employee> listOfEmpDetails();

	public Employee findByempDetail(Long empId);
	
	public int saveEmployeeDetails(Employee employee);
	
	public List<EmployeeAddress> saveEmployeeAddressDetails(Employee employee);

}

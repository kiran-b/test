package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hcl.dao.EmployeeDao;
import com.hcl.model.Employee;
import com.hcl.model.EmployeeAddress;

@Component
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDao employeeDao;

	public List<Employee> listOfEmpDetails() {
		// TODO Auto-generated method stub
		return employeeDao.listOfEmpDetails();

	}

	public Employee findByempDetail(Long empId) {
		return employeeDao.findByempDetail(empId);
	}

	public int saveEmployeeDetails(Employee employee) {
		return employeeDao.saveEmployeeDetails(employee);

	}

	public List<EmployeeAddress> saveEmployeeAddressDetails(Employee employee) {
		employeeDao.saveEmployeeDetails(employee);
		Long empId = employeeDao.getEmployeeId();

		List<EmployeeAddress> empAdd=employeeDao.saveEmployeeAddressDetails(employee, empId);
		return empAdd;

	}

}
